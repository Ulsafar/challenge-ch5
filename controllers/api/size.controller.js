const db = require('../../models')
const tbl_size = db.tbl_sizes;

async function getAllSize(req, res) {
    try{
        let size = await tbl_sizes.findAll()
    
        res.status(200).json({
            message:"success",
            data: size
        })
    }
    catch(error){
        console.log(error);
    }
}

module.exports = {
    getAllSize
}